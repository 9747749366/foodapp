import axios from 'axios'
import config from './config'
import * as types from '../constants'
import _ from 'lodash'

export const foodListAction = () => {

    return (dispatch) => {
        dispatch({
            type: types.FOOD_FETCH_REQUEST,
        })
        axios.get(config.SERVER_API_URL+"food" )
            .then(function (response) {
                dispatch({
                    type: types.FOOD_FETCH_SUCCESS,
                    response
                })
            })
            .catch(function (error) {
                dispatch({
                    type: types.FOOD_FETCH_FAILED,
                    error
                })

            });
    };

}


export const foodAddAction = (payload) => {
    return (dispatch) => {
        dispatch({
            type: types.FOOD_ADD_REQUEST,
        })
        axios.post(config.SERVER_API_URL+"food/",payload )
            .then(function (response) {
                dispatch({
                    type: types.FOOD_ADD_SUCCESS,
                    response
                })
                dispatch(foodListAction())
            })
            .catch(function (error) {
                dispatch({
                    type: types.FOOD_ADD_FAILED,
                    error
                })
            });
    };

}

export const foodDeleteAction = (id) => {
    return (dispatch) => {
        dispatch({
            type: types.FOOD_DELETE_REQUEST,
        })
        axios.delete(config.SERVER_API_URL+"food/"+id+"/" )
            .then(function (response) {
                dispatch({
                    type: types.FOOD_DELETE_SUCCESS,
                    response
                })
                dispatch(foodListAction());
            })
            .catch(function (error) {
                dispatch({
                    type: types.FOOD_DELETE_FAILED,
                    error
                })
            });
    };

}
