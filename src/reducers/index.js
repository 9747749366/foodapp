import { combineReducers } from 'redux';
import foodReducer from './foodReducer';
const main = combineReducers({
  foodReducer
})
export default combineReducers({
  main
})
